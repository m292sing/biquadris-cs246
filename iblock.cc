#include <iostream>
#include <string>
#include <vector>
#include "iblock.h"
#include <memory>
using namespace std;


IBlock::IBlock() {
  heavy = false;
  rotation = 0;
  block_type ='I';
  for(int i = 0 ; i < 4; i ++) {
    std::vector<char> c2;
     for(int j = 0 ; j < 4 ; j++) {
         if(i == 3) {
           c2.push_back('I');
    
          } else {
            c2.push_back(' ');
          }
      }
      v.push_back(c2);
   }
}

char IBlock::getName() { return block_type;}
vector<vector<char> > IBlock::getBlock() {
  return this->v;
}

void IBlock::print() {
  for(int i = 0 ; i < 4; i ++) {
     for(int j = 0 ; j < 4 ; j++) {
         cout << v[i][j];
      }
    std::cout << std::endl;
   }

}

void IBlock::setHeavy() {
  heavy = true;
}

void IBlock::unsetHeavy() {
  heavy = false;
}

bool IBlock::isHeavy() {
  return heavy;
}

void IBlock::setHeavyL3() {
  heavyL3 = true;
}

void IBlock::unsetHeavyL3() {
  heavyL3 = false;
}

bool IBlock::isHeavyL3() {
  return heavyL3;
}

int IBlock::rotateState() {
  return rotation;
}

void IBlock::setrotateState(int state ) {
  rotation = state;
}

IBlock::~IBlock() {}
