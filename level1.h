#ifndef LEVEL1_H
#define LEVEL1_H
#include "level.h"
#include <string>
#include <vector>
#include "block.h"
#include "player.h"
#include <memory>

class Level1: public Level {
    int l;
    int s;
    public:
    Level1(int s);
    void setLevel();
    virtual int getLevel();
    std::shared_ptr<Block> getBlock(Player *p) override;
    virtual ~Level1();
};

#endif
