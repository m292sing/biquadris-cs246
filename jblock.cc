#include <iostream>
#include <string>
#include <vector>
#include "jblock.h"
using namespace std;

JBlock::JBlock() {
  heavy = false;

  rotation = 0;
  block_type ='J';
  for(int i = 0 ; i < 4; i ++) {
    std::vector<char> c2;
     for(int j = 0 ; j < 4 ; j++) {
         if(i == 2 && j == 0) {
           c2.push_back('J');
    
          }
          else if (i == 3 && j != 3) {
            c2.push_back('J');
          }
          else {
            c2.push_back(' ');
          }
      }
      v.push_back(c2);
   }
}

char JBlock::getName() { return block_type;}

vector<vector<char> > JBlock::getBlock() {
  return this->v;
}

void JBlock::print() {
  for(int i = 0 ; i < 4; i ++) {
     for(int j = 0 ; j < 4 ; j++) {
         cout << v[i][j];
      }
    std::cout << std::endl;
   }

}

void JBlock::setHeavy() {
  heavy = true;
}
void JBlock::unsetHeavy() {
  heavy = false;
}

bool JBlock::isHeavy() {
  return heavy;
}

void JBlock::setHeavyL3() {
  heavyL3 = true;
}

void JBlock::unsetHeavyL3() {
  heavyL3 = false;
}

bool JBlock::isHeavyL3() {
  return heavyL3;
}

int JBlock::rotateState() {
  return rotation;
}

void JBlock::setrotateState(int state ) {
  rotation = state;
}

JBlock::~JBlock() {v.clear();}
