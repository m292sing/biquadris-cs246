#ifndef LEVEL3_H
#define LEVEL3_H
#include "level.h"
#include <string>
#include <vector>
#include "block.h"
#include "player.h"
#include <memory>

class Level3: public Level {
    int l;
    int s;
    bool readIn;
    std::string file1;
    std::vector<char> seq1;
    int counter1; 
    public:
    Level3(int s,std::string,bool);
    void setLevel();
    void readFile(std::string);
    virtual int getLevel();
    std::shared_ptr<Block> getBlock(Player *p) override;
    virtual ~Level3();
};

#endif
