#include "level4.h"
#include "block.h"
#include "iblock.h"
#include "jblock.h"
#include "lblock.h"
#include "oblock.h"
#include "sblock.h"
#include "tblock.h"
#include "zblock.h"
#include "player.h"
#include <fstream>
#include <iostream>
#include "level.h"
#include <cstdlib>
#include <memory>
using namespace std;

Level4::Level4(int s,std::string file,bool r): s(s),readIn(r),file1(file) {
    l = 3;
    seq1.clear();
    counter1 = 0;
    if(readIn == true) {
      readFile(file1);
     }
     srand(s);
};

void Level4::setLevel() {
    l = 3;
}

int Level4::getLevel() {
    return l;
}

void Level4::readFile(string file) {
    ifstream filep1(file);
    char c1;
    while (filep1 >> c1) {
        seq1.push_back(c1);
    }
}


shared_ptr<Block> Level4::getBlock(Player *p) {
    shared_ptr<Block> b = nullptr;
    if(readIn == true) {
      if (seq1[counter1] == 'I') {
            b = make_shared<IBlock>();
        }
        else if (seq1[counter1] == 'J') {
            b = make_shared<JBlock>();
        }
        else if (seq1[counter1] == 'L') {
            b = make_shared<LBlock>();
        }
        else if (seq1[counter1] == 'O') {
            b = make_shared<OBlock>();
        }
        else if (seq1[counter1] == 'S') {
            b = make_shared<SBlock>();
        }
        else if (seq1[counter1] == 'T') {
            b = make_shared<TBlock>();
        }
        else if (seq1[counter1] == 'Z') {
             b = make_shared<ZBlock>();
         }
            ++counter1;
        if (counter1 == seq1.size()) {
            counter1 = 0;
        }
      b->setHeavyL3();
      return b;
    }
    int r = rand() % 9;
    if (r == 0 || r == 1) {
        b = make_shared<SBlock>();
    }
    else if (r == 2 || r == 3) {
        b = make_shared<ZBlock>();
    }
    else if (r == 4) {
        b = make_shared<IBlock>();
    }
    else if (r == 5) {
        b = make_shared<JBlock>();
    }
    else if (r == 6) {
        b = make_shared<LBlock>();
    }
    else if (r == 7) {
        b = make_shared<OBlock>();
    }
    else if (r == 8) {
        b = make_shared<TBlock>();
    }
    b->setHeavyL3();
    return b;
}

Level4::~Level4() {}
