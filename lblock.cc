#include <iostream>
#include <string>
#include <vector>
#include "lblock.h"
using namespace std;

LBlock::LBlock() {
  heavy = false;
  rotation = 0;
  block_type ='L';
  for(int i = 0 ; i < 4; i ++) {
    std::vector<char> c2;
     for(int j = 0 ; j < 4 ; j++) {
         if(i == 2 && j == 2) {
           c2.push_back('L');
          }
          else if (i == 3 && j != 3) {
            c2.push_back('L');
          }
          else {
            c2.push_back(' ');
          }
      }
      v.push_back(c2);
   }
}
char LBlock::getName() { return block_type;}

vector<vector<char> > LBlock::getBlock() {
  return this->v;
}

void LBlock::print() {
  for(int i = 0 ; i < 4; i ++) {
     for(int j = 0 ; j < 4 ; j++) {
         cout << v[i][j];
      }
    std::cout << std::endl;
   }
}
void LBlock::setHeavy() {
  heavy = true;
}
void LBlock::unsetHeavy() {
  heavy = false;
}

bool LBlock::isHeavy() {
  return heavy;
}

void LBlock::setHeavyL3() {
  heavyL3 = true;
}

void LBlock::unsetHeavyL3() {
  heavyL3 = false;
}

bool LBlock::isHeavyL3() {
  return heavyL3;
}

int LBlock::rotateState() {
  return rotation;
}

void LBlock::setrotateState(int state ) {
  rotation = state;
}

LBlock::~LBlock() {v.clear();}
