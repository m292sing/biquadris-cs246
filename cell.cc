#include "cell.h"
#include <string>
#include <iostream>
using namespace std;

Cell::Cell(int xrow,int yrow): xrow(xrow), ycol(yrow), filled(0), word(' '), landed(0),blind(false) {}

bool Cell::checkEmpty() {
    if (!filled) {
        return true;
    }
    return false;
}

 bool Cell::checkLanded() {//only returns true after drop function is called
     if(landed) {
         return true;
     }
     return false;
 }

int Cell::getRow() {
    return xrow;
}

int Cell::getCol() {
    return ycol;
}

void Cell::reset() {
    word = ' ';
    filled = 0;
    landed = 0;
}

void Cell::setCell(const char &b) {
    word = b;
    filled = 1;
}

void Cell::setLanded(int n) {
  landed = n;
}

void Cell::setBlind() {
            blind = true;
}

void Cell::unsetBlind() {
            blind = false;
}

char Cell::getWord() {
  return word;
  
}

ostream& operator<< (ostream &out, const Cell& cell) {//add colour
      if (cell.blind) {
          out << '?';
      }
      else {
          out << cell.word;
      }
    return out;
}

Cell::~Cell() {}
