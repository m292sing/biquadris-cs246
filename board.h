#ifndef _BOARD_H
#define _BOARD_H
#include <string>
#include <vector>
#include "player.h"

class Board {

   Player *p1;
   Player *p2;
   int highscore = 0;
  public:
    Board();
    Player* getPlayer(int num);
    std::string mode;  
    int turn = 1;

    void updateScore(Player*);
    void setPlayer(Player*,int,std::string,int);
    void undrawBlock(Player*);
    int currPlayer();
    void drawBoard(Player*,Player*);
    bool drawBlock(Player*);
    void drawNextBlock(Player*,Player*);
    void moveRight(Player*);
    void moveLeft(Player*);
    void moveDown(Player*);
    void Drop(Player*);
    void DropStar(Player*);
    void setBlind(Player*);
    void unsetBlind(Player*);
    void clearLine(Player*);
    int clearLineRow(Player*);
    void makeblockHeavy(Player*);
    void checkHeavy(Player *);
    void Rotatecw(Player *);
    void Rotateccw(Player *);
    void setLevelup(Player*,int,std::string);
    void setLeveldown(Player*,int,std::string);
    bool isGameOver(Player*);
    void resetPlayer(Player*,int,std::string,int);
    void restart(Player*,Player*,std::string,std::string,int,int);
    bool gameOver(Player*,Player*);
    ~Board();

};



#endif
