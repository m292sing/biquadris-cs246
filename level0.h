#ifndef LEVEL0_H
#define LEVEL0_H
#include "level.h"
#include <string>
#include <vector>
#include "block.h"
#include "player.h"
#include <memory>

class Level0: public Level {
    int l;
    std::string file1;
    std::vector<char> seq1;
    int counter1;
    public:
    Level0(std::string);
    void setLevel();
    virtual int getLevel();
    void readFile(std::string file1);
    std::shared_ptr<Block> getBlock(Player *p) override;
    virtual ~Level0();
};

#endif
