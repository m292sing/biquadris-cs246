#ifndef LEVEL4_H
#define LEVEL4_H
#include "level.h"
#include <string>
#include <vector>
#include "block.h"
#include "player.h"
#include <memory>

class Level4: public Level {
    int l;
    int s;
    bool readIn;
    std::string file1;
    std::vector<char> seq1;
    int counter1; 
    public:
    Level4(int s,std::string,bool);
    void readFile(std::string);
    void setLevel();
    virtual int getLevel();
    std::shared_ptr<Block> getBlock(Player *p) override; 
    virtual ~Level4();
};

#endif
