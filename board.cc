#include "board.h"
#include "player.h"
#include <iostream>
#include <vector>
#include "cell.h"
#include "block.h"
#include "level.h"
#include "level0.h"
#include "iblock.h"
#include "level1.h"
#include "level2.h"
#include "level3.h"
#include "level4.h"
using namespace std;

Board::Board() {
 p1 = new Player();
 p2 = new Player();
 mode = "text";
}



  // Spaces 
Player* Board::getPlayer(int num) {
  if(num == 1) {
      p1->setPlayerNum(num);
       return p1;
   } else {
      p2->setPlayerNum(num);
      return p2;
   }
}

int Board::currPlayer() {
  if(turn % 2 == 1) { return 1;}
  else {return 2;}
}

void Board::setPlayer(Player* p,int level,std::string file,int s) {
  for(int i = 0 ; i < 18 ; i++) {
    std::vector<Cell*> c1;
   for(int j = 0 ; j < 11; j++) {
      Cell *c = new Cell(i,j);
      c1.push_back(c);
    }
    p->vc.push_back(c1);
  }
  p->not_random = false;
  p->score = 0;
  p->count_lines = 0;
  p->count_blocks = 0;
  p->setLev(level);
  p->current = nullptr;
  p->next = nullptr;
  p->l = nullptr;
  if(level == 0) {
    p->l = new Level0(file);
  } else if (level == 1) {
    p->l = new Level1(s);
  } else if (level == 2) {
    p->l = new Level2(s);
  } else if (level == 3) {
    p->l = new Level3(s,file,p->not_random);
  } else if (level == 4) {
    p->l = new Level4(s,file,p->not_random);
  }
   

}


void Board::resetPlayer(Player* p, int level,std::string file,int s) {
  delete p->l;
  for(int i = 0 ; i < 18 ; i++) {
   for(int j = 0 ; j < 11; j++) {
      p->vc[i][j]->reset();
    }
  }
  p->score = 0;
  p->count_lines = 0;
  p->count_blocks = 0;
  p->not_random = false;
  p->setLev(level);
  p->current = nullptr;
  p->next = nullptr;
  p->l = nullptr;
  if(level == 0) {
    p->l = new Level0(file);
  } else if (level == 1) {
    p->l = new Level1(s);
  } else if (level == 2) {
    p->l = new Level2(s);
  } else if (level == 3) {
    p->l = new Level3(s,file,p->not_random);
  } else if (level == 4) {
    p->l = new Level4(s,file,p->not_random);
  }
   
}

void Board::setLevelup(Player* p,int s,std::string file) {
  int level = p->getLev();
  if (level == 4) {
    p->setLev(4);
  } else {
    delete p->l;
    if(level == 0) { p->l = new Level1(s);}
    else if(level == 1) {p->l = new Level2(s);}
    else if(level == 2) {
      p->l = new Level3(s,file,p->not_random);
      p->current->setHeavyL3();
      p->next->setHeavyL3();
    }
    else if(level == 3) {
    p->l = new Level4(s,file,p->not_random);
    p->current->setHeavyL3();
    p->next->setHeavyL3();
    }
    p->setLev(level + 1);
    
  }
}

void Board::setLeveldown(Player* p,int s,std::string file) {
  int level = p->getLev();
  if (level == 0) {
    p->setLev(0);
  } else {
    delete p->l;
    if(level == 1) { p->l = new Level0(file);}
    else if(level == 2) {p->l = new Level1(s);}
    else if(level == 3) {
      p->l = new Level2(s);
      p->current->unsetHeavyL3(); 
      p->next->unsetHeavyL3();
    }
    else if(level == 4) {p->l = new Level3(s,file,p->not_random);}
    p->setLev(level - 1);
  }
}

void Board::restart(Player* p1,Player* p2,std::string file1,std::string file2,int l,int s) {
  resetPlayer(p1,l,file1,s);
  resetPlayer(p2,l,file2,s);

  p1->current = p1->l->getBlock(p1);
  p1->next = p1->l->getBlock(p1);

  p2->current = p2->l->getBlock(p2);
  p2->next = p2->l->getBlock(p2);

}


bool Board::gameOver(Player *,Player*) {
  cout << endl << endl;
  cout << "Game over" << endl;
  cout << "Score for Player 1:" << p1->score << endl;
  cout << "Score for Player 2:" << p2->score << endl;
  char c;
  cout <<"Do you want to play again [Y/N] ?" << endl;
  cin >> c;
  if(c == 'Y' || c == 'y') {
    return true;
  } else {
    return false;
  }
  
}


void Board::setBlind(Player* p) {
  for (int i = 5; i < 15; ++i) {
    for (int j = 2; j < 9; ++j) {
      p->vc[i][j]->setBlind();
    }
  }
}

void Board::unsetBlind(Player* p) {
  for (int i = 5; i < 15; ++i) {
    for (int j = 2; j < 9; ++j) {
      p->vc[i][j]->unsetBlind();
    }
  }
}


void Board::drawBoard(Player* p1,Player* p2) {

 if(mode ==  "text") {
    if(p1->score >= p2->score) {
       if(highscore < p1->score) {
         highscore = p1->score;
       }
     } else {
        if(highscore < p2->score) {
          highscore = p2->score;
         }
     }
  std::cout << "             Highscore:" << highscore << std::endl;     
  std::cout << "Level:    " << p1->getLev() << "               " << "Level:    " << p2->getLev() << std::endl;
  std::cout << "Score:    " << p1->score << "               " << "Score:    " << p2->score << std::endl;
  std::cout << "-----------" << "               " << "-----------" << std::endl; 

  
  for(int i = 0; i < 18 ; i++) {
    for(int j = 0 ; j < 11; j++) {
      std::cout << *(p1->vc[i][j]);
    }
     std::cout <<"               ";
   for(int k = 0 ; k < 11 ; k++) {
       std::cout << *(p2->vc[i][k]);

   }
   std::cout << std::endl;
  }

  std::cout << "-----------" << "               " << "-----------" << std::endl;  

  std::cout << "Next:" << "                     " << "Next:" << std::endl; 


 drawNextBlock(p1,p2);
 } else {

  std::cout << "Entering graphics mode";
 }

}


Board::~Board() {
delete p1->l;
delete p2->l;
delete p1;
delete p2;
}

bool drawPoss(Player * p) {
int counter = 0;
std::vector<std::vector<char> > v = p->current->getBlock();

for(int i = 0 ; i < 4; i++) {
    for(int j = 0 ; j < 4 ; j++) {
       char c = v[i][j];
       if(c != ' '  && p->vc[i][j]->checkEmpty()) {
         //p->vc[i][j]->setCell(c);
         counter++;
        }

     }

  }

if(counter != 4) { return false;}
return true;
}


void Board::undrawBlock(Player *p) {
  int count = 0;
    for(int row = 0 ; row < 18; row++) {
      for(int col = 0 ; col < 11 ; col++) {
       if(!(p->vc[row][col]->checkLanded()) && !(p->vc[row][col]->checkEmpty())) {
            p->vc[row][col]->reset();
            //p->vc[row][col-1]->setCell(c);
            count++;
        }

     }
     if(count == 4) { break;}
  }


}


bool Board::drawBlock(Player* p) {
  int counter = 0;
  std::vector<std::vector<char> > v = p->current->getBlock();
 if(drawPoss(p)) {
  for(int i = 0 ; i < 4; i++) {
    for(int j = 0 ; j < 4 ; j++) {
       char c = v[i][j];
       if(c != ' '  && p->vc[i][j]->checkEmpty()) {
         p->vc[i][j]->setCell(c);
         counter++;
        }

     }

  }

 return true;
} else {

  return false;

}



}




void Board::drawNextBlock(Player* p1 , Player* p2) {
  std::vector<std::vector<char> > v1 = p1->next->getBlock();
  std::vector<std::vector<char> > v2 = p2->next->getBlock();

  for(int i = 0 ; i < 4; i++ ) {
   for(int j = 0 ; j < 4 ; j++) {
      std::cout << v1[i][j];
   }
    std::cout <<"                      ";
   for(int k = 0 ; k < 4 ; k++) {
     std::cout << v2[i][k];

   }

    std::cout << std::endl;
  }


}



void Board::moveLeft(Player* p) {
  std::vector<std::vector<char> > v = p->current->getBlock();
  char c = p->current->getName();
  int count = 0;
  if(p->checkLeft()) {
   for(int row = 0 ; row < 18; row++) {
      for(int col = 0 ; col < 11 ; col++) {
       //char c = v[i][j];
       if(!(p->vc[row][col]->checkLanded()) && !(p->vc[row][col]->checkEmpty())) {
            p->vc[row][col]->reset();
            p->vc[row][col-1]->setCell(c);
            count++;
        }

     }
     if(count == 4) { break;}
  }

  }

  checkHeavy(p);

}


void Board::moveRight(Player *p) {


  char c = p->current->getName();
  int count = 0;
  if(p->checkRight()) {
   for(int row = 17 ; row >= 0 ; row--) {
      for(int col = 10 ; col>= 0 ; col--) {
       if(!(p->vc[row][col]->checkLanded()) && !(p->vc[row][col]->checkEmpty())) {
           p->vc[row][col]->reset();
           p->vc[row][col+1]->setCell(c);
        }

     }
     if(count == 4) { break;}
  }
 }


  checkHeavy(p);  

}

void Board::DropStar(Player *p) {
  for (int i = 0; i < 18; ++i) {
    if (p->vc[i][5]->checkLanded() == true) {
      cout << i << endl;
      p->vc[i-1][5]->setCell('*');
      p->vc[i-1][5]->setLanded(1);
      break;
    }
    if (i == 17) {
      p->vc[17][5]->setCell('*');
      p->vc[17][5]->setLanded(1);
    }
  }
}

void Board::Drop(Player*p) {

int count = 0;
  while(p->checkDown()) {
    moveDown(p);
  }

 for(int row = 17 ; row >= 0 ; row--) {
      for(int col = 10; col>=0; col--) {
        if(!(p->vc[row][col]->checkLanded()) && !(p->vc[row][col]->checkEmpty())) {
             p->vc[row][col]->setLanded(1);
        }
      }
      if(count == 4) { break;}
      }


  clearLine(p);

 if(p->count_lines > 0) {
  updateScore(p);
 }
 if (p->count_lines == 0 && p->getLev() == 4) {
   ++p->count_blocks;
 }
 if(p->count_lines == 0 && p->count_blocks == 5 && p->getLev() == 4) {
   DropStar(p);
   p->count_blocks = 0;
 }
}


void Board::moveDown(Player *p) {
  char c = p->current->getName();
  int count = 0;
  if(p->checkDown()) {
    for(int row = 17 ; row >= 0 ; row--) {
      for(int col = 10; col>=0; col--) {
        if(!(p->vc[row][col]->checkLanded()) && !(p->vc[row][col]->checkEmpty())) {
           p->vc[row][col]->reset();
           p->vc[row+1][col]->setCell(c);
        }
      }
      if(count == 4) { break;}
      }
  }
}


void Board::clearLine(Player *p) {
  int index = clearLineRow(p);

  if(index == -1) {return;}
  if(index != -1) {   
   for(int column = 10; column >= 0; column --) {
          p->vc[index][column]->reset();
        }

        for (int i = index-1; i>= 0;i--) {
          for (int j = 10; j >= 0; j--) {
            if (p->vc[i][j]->checkLanded()) {
              char c;
              c = p->vc[i][j]->getWord();
              p->vc[i][j]->reset();
              p->vc[i+1][j]->setCell(c);
              p->vc[i+1][j]->setLanded(1);
            }
          }
       } 
   
   p->count_lines++;
  }

  clearLine(p);
}

int Board::clearLineRow(Player *p) {

  for(int row = 17; row >= 0; row--) {
    int filled = 0;
    for(int col = 10; col >= 0; col --) {
      if (p->vc[row][col]->checkLanded()) {
        filled+=1;
      }
      if (filled == 11) {
         return row;
       }
    }
  }
  return -1;
}


void Board::makeblockHeavy(Player* p) {
  p->current->setHeavy();
  p->count_lines = 0;
}


void Board::checkHeavy(Player *p) {
if (p->current->isHeavyL3()) {
    if(p->checkDown()) {
      moveDown(p);
    }
  }
  
 if(p->current->isHeavy()) {

    if(p->checkDown()) { // if down possible
      moveDown(p);
      if(p->checkDown()) { // 2nd down possible
        moveDown(p);
      } else  {
        Drop(p);
        p->current->unsetHeavy();
        p->current = p->next;
        p->next = p->l->getBlock(p);
        drawBlock(p);
        turn++;
      }
    }  else {
        Drop(p); // switch turn
        p->current->unsetHeavy();
        p->current = p->next;
        p->next = p->l->getBlock(p);
        drawBlock(p);
        turn++;
    }

  }

}

bool Board::isGameOver(Player *p) {
  for(int i = 0 ; i < 3 ; i++) {
    for(int j = 0 ; j < 11 ; j++ ) {
      if(p->vc[i][j]->checkLanded()){

         return true;
     }
   }

  }

return false;

}


void Board::updateScore(Player* p) {

  p->score = p->score + (p->count_lines + p->getLev())*(p->count_lines + p->getLev());
  //p->count_lines = 0;
}

void Board::Rotateccw(Player *p) {
  char c = p->current->getName();
  if (c == 'I' || c == 'S' || c == 'Z') {
    Board::Rotatecw(p);
    Board::Rotatecw(p);
    Board::Rotatecw(p);
  }
  else if (c == 'T' || c == 'J' || c == 'L') {
    if (c == 'T') {
      if (p->current->rotateState() == 0) {
        for (int i = 17; i>= 0; i--) {
          for (int j = 10; j >= 0; j--) {
            if(!(p->vc[i][j]->checkLanded()) && !(p->vc[i][j]->checkEmpty())) {
              if (i -2 >= 0) {
                if ((p->vc[i][j-1]->checkEmpty()) && (p->vc[i-2][j-1]->checkEmpty())) { 
                  p->vc[i][j]->reset();
                  p->vc[i-1][j+1]->reset();
                  p->vc[i][j-1]->setCell(c);
                  p->vc[i-2][j-1]->setCell(c);
                  p->current->setrotateState(3);
                  return;
                }
                else {
                  return;
                }
              }
            }
        }
      }
    }
      if (p->current->rotateState() == 3) {
        for (int i = 17; i>= 0; i--) {
          for (int j = 10; j >= 0; j--) {
            if(!(p->vc[i][j]->checkLanded()) && !(p->vc[i][j]->checkEmpty())) {
              if (j + 2 <= 10) {
                if ((p->vc[i][j+1]->checkEmpty()) && (p->vc[i][j+2]->checkEmpty())) { 
                  p->vc[i-1][j]->reset();
                  p->vc[i-2][j]->reset();
                  p->vc[i][j+1]->setCell(c);
                  p->vc[i][j+2]->setCell(c);
                  p->current->setrotateState(2);
                  return;
              }
              else {
                return;
              }

            }
          }
       }
    }
      }
      if (p->current->rotateState() == 2) {
        for (int i = 17; i>= 0; i--) {
          for (int j = 10; j >= 0; j--) {
            if(!(p->vc[i][j]->checkLanded()) && !(p->vc[i][j]->checkEmpty())) {
              if (i -2 >= 0) {
                if ((p->vc[i-1][j-2]->checkEmpty()) && (p->vc[i-2][j-1]->checkEmpty())) { 
                  p->vc[i][j]->reset();
                  p->vc[i][j-2]->reset();
                  p->vc[i-1][j-2]->setCell(c);
                  p->vc[i-2][j-1]->setCell(c);
                  p->current->setrotateState(1);
                  return;
              }
              else {
                return;
              }
            }
          }
       }
    }
      }
      if (p->current->rotateState() == 1) {
        for (int i = 17; i>= 0; i--) {
          for (int j = 10; j >= 0; j--) {
            if(!(p->vc[i][j]->checkLanded()) && !(p->vc[i][j]->checkEmpty())) {
              if (j + 1 <= 10) {
                if ((p->vc[i-1][j+1]->checkEmpty())) { 
                  p->vc[i-2][j]->reset();
                  p->vc[i-1][j+1]->setCell(c);
                  p->current->setrotateState(0);
                  return;
              }
              else {
                return;
              }

            }
          }
       }
    }
      }
    }
else if (c == 'J') {
      if (p->current->rotateState() == 0) {
        for (int i = 17; i>= 0; i--) {
          for (int j = 10; j >= 0; j--) {
            if(!(p->vc[i][j]->checkLanded()) && !(p->vc[i][j]->checkEmpty())) {
              if (i -2 >= 0) {
                if ((p->vc[i-2][j-1]->checkEmpty()) && (p->vc[i-1][j-1]->checkEmpty())) { 
                  p->vc[i][j]->reset();
                  p->vc[i-1][j-2]->reset();
                  p->vc[i-1][j-1]->setCell(c);
                  p->vc[i-2][j-1]->setCell(c);
                  p->current->setrotateState(3);
                  return;
              }
              else {
                return;
              }

            }
          }
       }
    }
      }
      if (p->current->rotateState() == 3) {
        for (int i = 17; i>= 0; i--) {
          for (int j = 10; j >= 0; j--) {
            if(!(p->vc[i][j]->checkLanded()) && !(p->vc[i][j]->checkEmpty())) {
              if (j + 1 <= 10) {
                if ((p->vc[i-1][j-1]->checkEmpty()) && (p->vc[i-1][j+1]->checkEmpty()) && (p->vc[i][j+1]->checkEmpty())) { 
                  p->vc[i][j]->reset();
                  p->vc[i][j-1]->reset();
                  p->vc[i-2][j]->reset();
                  p->vc[i-1][j-1]->setCell(c);
                  p->vc[i-1][j+1]->setCell(c);
                  p->vc[i][j+1]->setCell(c);
                  p->current->setrotateState(2);
                  return;
              }
              else {
                return;
              }

            }
          }
       }
    }
      }
      if (p->current->rotateState() == 2) {
        for (int i = 17; i>= 0; i--) {
          for (int j = 10; j >= 0; j--) {
            if(!(p->vc[i][j]->checkLanded()) && !(p->vc[i][j]->checkEmpty())) {
              if (i -2 >= 0) {
                if ((p->vc[i][j-2]->checkEmpty()) && (p->vc[i-2][j-1]->checkEmpty()) && (p->vc[i-2][j-2]->checkEmpty())) { 
                  p->vc[i][j]->reset();
                  p->vc[i-1][j]->reset();
                  p->vc[i-1][j-1]->reset();
                  p->vc[i][j-2]->setCell(c);
                  p->vc[i-2][j-1]->setCell(c);
                  p->vc[i-2][j-2]->setCell(c);
                  p->current->setrotateState(1);
                  return;
              }
              else {
                return;
              }

            }
          }
       }
    }
      }
    if (p->current->rotateState() == 1) {
        for (int i = 17; i>= 0; i--) {
          for (int j = 10; j >= 0; j--) {
            if(!(p->vc[i][j]->checkLanded()) && !(p->vc[i][j]->checkEmpty())) {
              if (j + 2 <= 10) {
                if ((p->vc[i][j+1]->checkEmpty()) && (p->vc[i][j+2]->checkEmpty())) { 
                  p->vc[i-2][j]->reset();
                  p->vc[i-2][j+1]->reset();
                  p->vc[i][j+1]->setCell(c);
                  p->vc[i][j+2]->setCell(c);
                  p->current->setrotateState(0);
                  return;
              }
              else {
                return;
              }

            }
          }
       }
    }
      }
}
    else {//c=='L'
      if (p->current->rotateState() == 0) {
        for (int i = 17; i>= 0; i--) {
          for (int j = 10; j >= 0; j--) {
            if(!(p->vc[i][j]->checkLanded()) && !(p->vc[i][j]->checkEmpty())) {
              if (i -2 >= 0) {
                if ((p->vc[i-1][j-1]->checkEmpty()) && (p->vc[i-2][j-1]->checkEmpty()) && (p->vc[i-2][j-2]->checkEmpty())) { 
                  p->vc[i][j]->reset();
                  p->vc[i-1][j]->reset();
                  p->vc[i][j-2]->reset();
                  p->vc[i-1][j-1]->setCell(c);
                  p->vc[i-2][j-1]->setCell(c);
                  p->vc[i-2][j-2]->setCell(c);
                  p->current->setrotateState(3);
                  return;
              }
              else {
                return;
              }

            }
          }
       }
    }
      }
      if (p->current->rotateState() == 3) {
        for (int i = 17; i>= 0; i--) {
          for (int j = 10; j >= 0; j--) {
            if(!(p->vc[i][j]->checkLanded()) && !(p->vc[i][j]->checkEmpty())) {
              if (j + 1 <= 10) {
                if ((p->vc[i-1][j-1]->checkEmpty()) && (p->vc[i-1][j+1]->checkEmpty()) && (p->vc[i][j-1]->checkEmpty())) { 
                  p->vc[i][j]->reset();
                  p->vc[i-2][j]->reset();
                  p->vc[i-2][j-1]->reset();
                  p->vc[i-1][j-1]->setCell(c);
                  p->vc[i-1][j+1]->setCell(c);
                  p->vc[i][j-1]->setCell(c);
                  p->current->setrotateState(2);
                  return;
              }
              else {
                return;
              }

            }
          }
       }
    }
      }
      if (p->current->rotateState() == 2) {
        for (int i = 17; i>= 0; i--) {
          for (int j = 10; j >= 0; j--) {
            if(!(p->vc[i][j]->checkLanded()) && !(p->vc[i][j]->checkEmpty())) {
              if (i -2 >= 0) {
                if ((p->vc[i-2][j]->checkEmpty()) && (p->vc[i][j+1]->checkEmpty())) { 
                  p->vc[i-1][j+1]->reset();
                  p->vc[i-1][j+2]->reset();
                  p->vc[i-2][j]->setCell(c);
                  p->vc[i][j+1]->setCell(c);
                  p->current->setrotateState(1);
                  return;
              }
              else {
                return;
              }

            }
          }
       }
    }
      }
      if (p->current->rotateState() == 1) {
        for (int i = 17; i>= 0; i--) {
          for (int j = 10; j >= 0; j--) {
            if(!(p->vc[i][j]->checkLanded()) && !(p->vc[i][j]->checkEmpty())) {
              if (j + 1 <= 10) {
                if ((p->vc[i][j+1]->checkEmpty()) && (p->vc[i-1][j+1]->checkEmpty())) { 
                  p->vc[i-1][j-1]->reset();
                  p->vc[i-2][j-1]->reset();
                  p->vc[i][j+1]->setCell(c);
                  p->vc[i-1][j+1]->setCell(c);
                  p->current->setrotateState(0);
                  return;
              }
              else {
                return;
              }

            }
          }
       }
    }
      }
    }
  }
  else {
    return;
  }
}

void Board::Rotatecw(Player *p) {
  char c = p->current->getName();
  if (c == 'I') {
    if (p->current->rotateState() == 1) {
    for (int i = 17; i>= 0; i--) {
          for (int j = 10; j >= 0; j--) {
            if(!(p->vc[i][j]->checkLanded()) && !(p->vc[i][j]->checkEmpty())) {
              if (j+3 <= 10) {
                if ((p->vc[i][j+1]->checkEmpty()) && (p->vc[i][j+2]->checkEmpty()) && (p->vc[i][j+3]->checkEmpty())) {
                  //checkforlanded? 
                  p->vc[i-1][j]->reset();
                  p->vc[i-2][j]->reset();
                  p->vc[i-3][j]->reset();
                  p->vc[i][j+1]->setCell(c);
                  p->vc[i][j+2]->setCell(c);
                  p->vc[i][j+3]->setCell(c);
                  p->current->setrotateState(0);
                  return;
              }
              else {
                return;
              }

            }
          }
       }
    }
    }
    else {
      for (int i = 0; i < 18; i++) {
        for (int j = 0; j < 11; j++) {
          if(!(p->vc[i][j]->checkLanded()) && !(p->vc[i][j]->checkEmpty())) {
            if (i-3 >= 0) {
              if ((p->vc[i-1][j]->checkEmpty()) && (p->vc[i-2][j]->checkEmpty()) && (p->vc[i-3][j]->checkEmpty())) {
                  p->vc[i][j+1]->reset();
                  p->vc[i][j+2]->reset();
                  p->vc[i][j+3]->reset();
                  p->vc[i-1][j]->setCell(c);
                  p->vc[i-2][j]->setCell(c);
                  p->vc[i-3][j]->setCell(c);
                  p->current->setrotateState(1);
                  return;
              }
              else {
                return;
              }
            }

          }
        }
      }
  }
}
  else if (c == 'S') {
    if (p->current->rotateState() == 0) {//horizontal
    for (int i = 0; i< 18; i++) {
          for (int j = 0; j < 11; j++) {
            if(!(p->vc[i][j]->checkLanded()) && !(p->vc[i][j]->checkEmpty())) {
              if (i-1 >= 0) {
                if ((p->vc[i][j-1]->checkEmpty()) && (p->vc[i-1][j-1]->checkEmpty())) {
                  //checkforlanded? 
                  p->vc[i+1][j-1]->reset();
                  p->vc[i][j+1]->reset();
                  p->vc[i][j-1]->setCell(c);
                  p->vc[i-1][j-1]->setCell(c);
                  p->current->setrotateState(1);
                  return;
              }
              else {
                return;
              }
            }
          }
       }
    }
    }
    else {
      for (int i = 17; i >=0; i--) {
          for (int j = 10; j >= 0; j--) {
            if(!(p->vc[i][j]->checkLanded()) && !(p->vc[i][j]->checkEmpty())) {
              if (j+1 <= 10) {
                if ((p->vc[i][j-1]->checkEmpty()) && (p->vc[i-1][j+1]->checkEmpty())) {
                  p->vc[i-1][j-1]->reset();
                  p->vc[i-2][j-1]->reset();
                  p->vc[i][j-1]->setCell(c);
                  p->vc[i-1][j+1]->setCell(c);
                  p->current->setrotateState(0);
                  return;
              }
              else {
                return;
              }
            }
          }
       }
    }
    }
  }
  else if (c == 'Z') {
    if (p->current->rotateState() == 0) {
    for (int i = 17; i >=0; i--) {
          for (int j = 10; j >= 0; j--) {
            if(!(p->vc[i][j]->checkLanded()) && !(p->vc[i][j]->checkEmpty())) {
              if (i-2 >= 0) {
                if ((p->vc[i][j-2]->checkEmpty()) && (p->vc[i-2][j-1]->checkEmpty())) {
                  p->vc[i][j]->reset();
                  p->vc[i][j-1]->reset();
                  p->vc[i][j-2]->setCell(c);
                  p->vc[i-2][j-1]->setCell(c);
                  p->current->setrotateState(1);
                  return;
              }
              else {
                return;
              }
            }
          }
       }
    }
    }
    else {
      for (int i = 17; i >=0; i--) {
          for (int j = 10; j >= 0; j--) {
            if(!(p->vc[i][j]->checkLanded()) && !(p->vc[i][j]->checkEmpty())) {
              if (j+2 <= 10) {
                if ((p->vc[i][j+1]->checkEmpty()) && (p->vc[i][j+2]->checkEmpty())) {
                  p->vc[i][j]->reset();
                  p->vc[i-2][j+1]->reset();
                  p->vc[i][j+1]->setCell(c);
                  p->vc[i][j+2]->setCell(c);
                  p->current->setrotateState(0);
                  return;
              }
              else {
                return;
              }
            }
          }
       }
    }
    }
  }
  else if (c == 'L') {
    if (p->current->rotateState() == 0) {
    for (int i = 17; i >=0; i--) {
          for (int j = 10; j >= 0; j--) {
            if(!(p->vc[i][j]->checkLanded()) && !(p->vc[i][j]->checkEmpty())) {
              if (i-2 >= 0) {
                if ((p->vc[i-2][j-2]->checkEmpty()) && (p->vc[i-1][j-2]->checkEmpty())) {
                  p->vc[i][j]->reset();
                  p->vc[i-1][j]->reset();
                  p->vc[i-2][j-2]->setCell(c);
                  p->vc[i-1][j-2]->setCell(c);
                  p->current->setrotateState(1);
                  return;
              }
              else {
                return;
              }
            }
          }
       }
    }
    }
    else if (p->current->rotateState() == 1) {
    for (int i = 17; i >=0; i--) {
          for (int j = 10; j >= 0; j--) {
            if(!(p->vc[i][j]->checkLanded()) && !(p->vc[i][j]->checkEmpty())) {
              if (j+1 <= 10) {
                if ((p->vc[i-1][j]->checkEmpty()) && (p->vc[i-1][j+1]->checkEmpty())) {
                  p->vc[i][j]->reset();
                  p->vc[i-2][j-1]->reset();
                  p->vc[i-1][j]->setCell(c);
                  p->vc[i-1][j+1]->setCell(c);
                  p->current->setrotateState(2);
                  return;
              }
              else {
                return;
              }
            }
          }
       }
    }
    }
    else if (p->current->rotateState() == 2) {
    for (int i = 17; i >=0; i--) {
          for (int j = 10; j >= 0; j--) {
            if(!(p->vc[i][j]->checkLanded()) && !(p->vc[i][j]->checkEmpty())) {
              if (i-2 >= 0) {
                if ((p->vc[i-2][j]->checkEmpty()) && (p->vc[i-2][j+1]->checkEmpty()) && (p->vc[i][j+1]->checkEmpty())) {
                  p->vc[i][j]->reset();
                  p->vc[i-1][j]->reset();
                  p->vc[i-1][j+2]->reset();
                  p->vc[i-2][j+1]->setCell(c);
                  p->vc[i-2][j]->setCell(c);
                  p->vc[i][j+1]->setCell(c);
                  p->current->setrotateState(3);
                  return;
              }
              else {
                return;
              }
            }
          }
       }
    }
    }
    else if (p->current->rotateState() == 3) {
    for (int i = 17; i >=0; i--) {
          for (int j = 10; j >= 0; j--) {
            if(!(p->vc[i][j]->checkLanded()) && !(p->vc[i][j]->checkEmpty())) {
              if (j+1 <= 10) {
                if ((p->vc[i][j+1]->checkEmpty()) && (p->vc[i][j-1]->checkEmpty()) && (p->vc[i-1][j+1]->checkEmpty())) {
                  p->vc[i-1][j]->reset();
                  p->vc[i-2][j]->reset();
                  p->vc[i-2][j-1]->reset();
                  p->vc[i][j+1]->setCell(c);
                  p->vc[i][j-1]->setCell(c);
                  p->vc[i-1][j+1]->setCell(c);
                  p->current->setrotateState(0);
                  return;
              }
              else {
                return;
              }
            }
          }
       }
    }
    }
  }
   else if (c == 'T') {
    if (p->current->rotateState() == 0) {
    for (int i = 17; i >=0; i--) {
          for (int j = 10; j >= 0; j--) {
            if(!(p->vc[i][j]->checkLanded()) && !(p->vc[i][j]->checkEmpty())) {
              if (i-2 >= 0) {
                if ((p->vc[i-2][j]->checkEmpty())) {
                  p->vc[i-1][j+1]->reset();
                  p->vc[i-2][j]->setCell(c);
                  p->current->setrotateState(1);
                  return;
              }
              else {
                return;
              }
            }
          }
       }
    }
    }
    else if (p->current->rotateState() == 1) {
    for (int i = 17; i >=0; i--) {
          for (int j = 10; j >= 0; j--) {
            if(!(p->vc[i][j]->checkLanded()) && !(p->vc[i][j]->checkEmpty())) {
              if (j+1 <= 10) {
                if ((p->vc[i][j+1]->checkEmpty()) && (p->vc[i][j-1]->checkEmpty())) {
                  p->vc[i-1][j-1]->reset();
                  p->vc[i-2][j]->reset();
                  p->vc[i][j+1]->setCell(c);
                  p->vc[i][j-1]->setCell(c);
                  p->current->setrotateState(2);
                  return;
              }
              else {
                return;
              }
            }
          }
       }
    }
    }
    else if (p->current->rotateState() == 2) {
    for (int i = 17; i >=0; i--) {
          for (int j = 10; j >= 0; j--) {
            if(!(p->vc[i][j]->checkLanded()) && !(p->vc[i][j]->checkEmpty())) {
              if (i-2 >= 0) {
                if ((p->vc[i-2][j-2]->checkEmpty()) && (p->vc[i-1][j-2]->checkEmpty())) {
                  p->vc[i][j]->reset();
                  p->vc[i][j-1]->reset();
                  p->vc[i-1][j-2]->setCell(c);
                  p->vc[i-2][j-2]->setCell(c);
                  p->current->setrotateState(3);
                  return;
              }
              else {
                return;
              }
            }
          }
       }
    }
    }
    else if (p->current->rotateState() == 3) {
    for (int i = 17; i >=0; i--) {
          for (int j = 10; j >= 0; j--) {
            if(!(p->vc[i][j]->checkLanded()) && !(p->vc[i][j]->checkEmpty())) {
              if (j+1 <= 10) {
                if ((p->vc[i][j+1]->checkEmpty()) && (p->vc[i-1][j+2]->checkEmpty())) {
                  p->vc[i][j]->reset();
                  p->vc[i-2][j]->reset();
                  p->vc[i][j+1]->setCell(c);
                  p->vc[i-1][j+2]->setCell(c);
                  p->current->setrotateState(0);
                  return;
              }
              else {
                return;
              }
            }
          }
       }
    }
    }
  }
  else if (c == 'J') {//Jblock
    if (p->current->rotateState() == 0) {
    for (int i = 17; i >=0; i--) {
          for (int j = 10; j >= 0; j--) {
            if(!(p->vc[i][j]->checkLanded()) && !(p->vc[i][j]->checkEmpty())) {
              if (i-2 >= 0) {
                if ((p->vc[i-2][j-1]->checkEmpty() && (p->vc[i-2][j-2]->checkEmpty()))) {
                  p->vc[i][j]->reset();
                  p->vc[i][j-1]->reset();
                  p->vc[i-2][j-1]->setCell(c);
                  p->vc[i-2][j-2]->setCell(c);
                  p->current->setrotateState(1);
                  return;
              }
              else {
                return;
              }
            }
          }
       }
    }
    }
    else if (p->current->rotateState() == 1) {
    for (int i = 17; i >=0; i--) {
          for (int j = 10; j >= 0; j--) {
            if(!(p->vc[i][j]->checkLanded()) && !(p->vc[i][j]->checkEmpty())) {
              if (j+2 <= 10) {
                if ((p->vc[i-1][j+1]->checkEmpty()) && (p->vc[i-1][j+2]->checkEmpty()) && (p->vc[i][j+2]->checkEmpty())) {
                  p->vc[i][j]->reset();
                  p->vc[i-2][j]->reset();
                  p->vc[i-2][j+1]->reset();
                  p->vc[i-1][j+1]->setCell(c);
                  p->vc[i-1][j+2]->setCell(c);
                  p->vc[i][j+2]->setCell(c);
                  p->current->setrotateState(2);
                  return;
              }
              else {
                return;
              }
            }
          }
       }
    }
    }
    else if (p->current->rotateState() == 2) {
    for (int i = 17; i >=0; i--) {
          for (int j = 10; j >= 0; j--) {
            if(!(p->vc[i][j]->checkLanded()) && !(p->vc[i][j]->checkEmpty())) {
              if (i-2 >= 0) {
                if ((p->vc[i][j-1]->checkEmpty()) && (p->vc[i][j-2]->checkEmpty()) && (p->vc[i-2][j-1]->checkEmpty())) {
                  p->vc[i][j]->reset();
                  p->vc[i-1][j-2]->reset();
                  p->vc[i-1][j]->reset();
                  p->vc[i][j-1]->setCell(c);
                  p->vc[i][j-2]->setCell(c);
                  p->vc[i-2][j-1]->setCell(c);
                  p->current->setrotateState(3);
                  return;
              }
              else {
                return;
              }
            }
          }
       }
    }
    }
    else if (p->current->rotateState() == 3) {
    for (int i = 17; i >=0; i--) {
          for (int j = 10; j >= 0; j--) {
            if(!(p->vc[i][j]->checkLanded()) && !(p->vc[i][j]->checkEmpty())) {
              if (j+1 <= 10) {
                if ((p->vc[i][j+1]->checkEmpty()) && (p->vc[i-1][j-1]->checkEmpty())) {
                  p->vc[i-1][j]->reset();
                  p->vc[i-2][j]->reset();
                  p->vc[i][j+1]->setCell(c);
                  p->vc[i-1][j-1]->setCell(c);
                  p->current->setrotateState(0);
                  return;
              }
              else {
                return;
              }
            }
          }
       }
    }
    }
  }
  else {//OBLOCK
    return;
  }
}



