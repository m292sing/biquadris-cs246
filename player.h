#ifndef _PLAYER_H
#define _PLAYER_H
#include <vector>
#include "cell.h"
#include <memory>
class Level;
class Block;

class Player {
   int level;
   int playerNum;
   public:
   int score = 0;
   int count_lines = 0;
   int count_blocks = 0;
   int high_score = 0;
   bool not_random = false;
   Level *l;
   std::vector <std::vector <Cell*> > vc;
   void setLev(int);
   int getLev();
   int getPlayerNum(); // Returns player num required in Level 
   void setPlayerNum(int num);
   std::shared_ptr<Block> current;
   std::shared_ptr<Block> next;
   bool checkLeft();
   bool checkRight();
   bool checkDown();
   bool checkCW();
   bool checkCCW();
   ~Player();
};
#endif
