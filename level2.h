#ifndef LEVEL2_H
#define LEVEL2_H
#include "level.h"
#include <string>
#include <vector>
#include "block.h"
#include "player.h"
#include <memory>

class Level2: public Level {
    int l;
    int s;
    public:
    Level2(int s);
    void setLevel();
    virtual int getLevel();
    std::shared_ptr<Block> getBlock(Player *p) override;
    virtual ~Level2();
};

#endif
