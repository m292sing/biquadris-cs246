#include "level0.h"
#include "block.h"
#include "iblock.h"
#include "jblock.h"
#include "lblock.h"
#include "oblock.h"
#include "sblock.h"
#include "tblock.h"
#include "zblock.h"
#include "player.h"
#include <fstream>
#include <iostream>
#include "level.h"
#include <memory>
using namespace std;


void Level0::setLevel() { l = 0;}

void Level0::readFile(string file) {
    ifstream filep1(file);
    char c1;
    while (filep1 >> c1) {
        seq1.push_back(c1);
    }
}

Level0::Level0(std::string file1) {
    l = 0;
    file1 = file1;
    seq1.clear();
    counter1 = 0;
    readFile(file1);
}

int Level0::getLevel() {
    return l;
}


shared_ptr<Block> Level0::getBlock(Player *p) {
    shared_ptr<Block> b = nullptr;
        if (seq1[counter1] == 'I') {
            b = make_shared<IBlock>();
        }
        else if (seq1[counter1] == 'J') {
            b = make_shared<JBlock>();
        }
        else if (seq1[counter1] == 'L') {
            b = make_shared<LBlock>();
        }
        else if (seq1[counter1] == 'O') {
            b = make_shared<OBlock>();
        }
        else if (seq1[counter1] == 'S') {
            b = make_shared<SBlock>();
        }
        else if (seq1[counter1] == 'T') {
            b = make_shared<TBlock>();
        }
        else if (seq1[counter1] == 'Z') {
             b = make_shared<ZBlock>();
         }
            ++counter1;
        if (counter1 == seq1.size()) {
            counter1 = 0;
        }
  return b;
}

Level0::~Level0() {}
