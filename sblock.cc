#include <iostream>
#include <string>
#include <vector>
#include "sblock.h"
using namespace std;

SBlock::SBlock() {
   heavy = false;

    rotation = 0;
    block_type = 'S';
    for(int i = 0 ; i < 4; i ++) {
    std::vector<char> c2;
     for(int j = 0 ; j < 4 ; j++) {
         if (i == 2 && (j == 1 || j == 2)) {
             c2.push_back('S');
         }
         else if (i == 3 && (j == 0 || j == 1)) {
             c2.push_back('S');
         }
         else {
             c2.push_back(' ');
        }
      }
      v.push_back(c2);
   }
}

char SBlock::getName() { return block_type;}

vector<vector<char> > SBlock::getBlock() {
  return this->v;
}


void SBlock::print() {
  for(int i = 0 ; i < 4; i ++) {
     for(int j = 0 ; j < 4 ; j++) {
         cout << v[i][j];
      }
    std::cout << std::endl;
   }

}

void SBlock::setHeavy() {
  heavy = true;
}
void SBlock::unsetHeavy() {
  heavy = false;
}

bool SBlock::isHeavy() {
  return heavy;
}

void SBlock::setHeavyL3() {
  heavyL3 = true;
}

void SBlock::unsetHeavyL3() {
  heavyL3 = false;
}

bool SBlock::isHeavyL3() {
  return heavyL3;
}

int SBlock::rotateState() {
  return rotation;
}

void SBlock::setrotateState(int state ) {
  rotation = state;
}

SBlock::~SBlock() {v.clear();}
