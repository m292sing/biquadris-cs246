#ifndef LEVEL_H
#define LEVEL_H
#include "block.h"
#include "player.h"
#include <memory>
class Level {
    int l;
public:
    virtual void setLevel() = 0;
    virtual int getLevel() = 0;
    virtual std::shared_ptr<Block> getBlock(Player*) = 0;
    virtual ~Level() = 0;
};

#endif
