#ifndef CELL_H
#define CELL_H
#include <string>

class Cell {
    int xrow;
    int ycol;
    bool filled;
    char word;
    bool landed;
    bool blind;

public:
    Cell(int,int);
    ~Cell();
    int getRow();
    int getCol();
    bool checkEmpty();
    bool checkLanded();
    void reset();
    void setCell(const char&);
    void setLanded(int);
    void setBlind();
    void unsetBlind();
    char getWord();   
    friend std::ostream& operator<<(std::ostream &, const Cell &);

};

#endif
