#ifndef BLOCK_H
#define BLOCK_H
#include <string>
#include <vector>

class Block {

    protected:
    std::vector <std::vector <char> > v;
    bool heavy;
    bool heavyL3 = false;
    public:
    Block();
    virtual char getName() = 0;
    virtual std::vector <std::vector <char> > getBlock() = 0;
    virtual void print() = 0;
    virtual bool isHeavy() = 0;
    virtual void setHeavy() = 0;
    virtual void unsetHeavy() = 0;
    virtual bool isHeavyL3() = 0;
    virtual void setHeavyL3() = 0;
    virtual void unsetHeavyL3() = 0;
    virtual int rotateState() = 0;
    virtual void setrotateState(int) = 0;
    virtual ~Block();
};

#endif
