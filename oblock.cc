#include <iostream>
#include <string>
#include <vector>
#include "oblock.h"
using namespace std;

OBlock::OBlock() {
  heavy = false;
  rotation = 0;
  block_type ='O';
  for(int i = 0 ; i < 4; i ++) {
    std::vector<char> c2;
     for(int j = 0 ; j < 4 ; j++) {
         if (i == 2 && (j == 0 || j == 1)) { // j
             c2.push_back('O');
         }
         else if (i == 3 && (j == 0 || j == 1)) { // j
             c2.push_back('O');
         }
         else {
             c2.push_back(' ');
         }
      }
      v.push_back(c2);
   }
}

char OBlock::getName() { return block_type;}

vector<vector<char> > OBlock::getBlock() {
  return this->v;
}


void OBlock::print() {
  for(int i = 0 ; i < 4; i ++) {
     for(int j = 0 ; j < 4 ; j++) {
         cout << v[i][j];
      }
    std::cout << std::endl;
   }
}

void OBlock::setHeavy() {
  heavy = true;
}
void OBlock::unsetHeavy() {
  heavy = false;
}

bool OBlock::isHeavy() {
  return heavy;
}

void OBlock::setHeavyL3() {
  heavyL3 = true;
}

void OBlock::unsetHeavyL3() {
  heavyL3 = false;
}

bool OBlock::isHeavyL3() {
  return heavyL3;
}

int OBlock::rotateState() {
  return rotation;
}

void OBlock::setrotateState(int state ) {
  rotation = state;
}

OBlock::~OBlock() {v.clear();}
