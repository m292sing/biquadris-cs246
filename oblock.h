#ifndef OBLOCK_H
#define OBLOCK_H
#include "block.h"
#include <string>
#include <vector>

class OBlock: public Block{
    char block_type;
    int rotation;
    protected:
    std::vector <std::vector <char> > v;
    public:
    OBlock();
    char getName() override;
    std::vector <std::vector <char> > getBlock() override;
    void print() override;
    bool isHeavy();
    void setHeavy();
    void unsetHeavy();
    bool isHeavyL3();
    void setHeavyL3();
    void unsetHeavyL3();
    int rotateState();
    void setrotateState(int);
    ~OBlock();
};

#endif
