#include <iostream>
#include <string>
#include <vector>
#include "tblock.h"
using namespace std;

TBlock::TBlock() {
   heavy = false;

    rotation = 0;
    block_type = 'T';
    for(int i = 0 ; i < 4; i ++) {
        std::vector<char> c2;
        for(int j = 0 ; j < 4 ; j++) {
            if (i == 2 && j != 3) {
                c2.push_back('T');
            }
            else if (i == 3 && j == 1) {
                c2.push_back('T');
            }
            else {
                c2.push_back(' ');
            }
        }
        v.push_back(c2);
    }
}

char TBlock::getName() { return block_type;}

vector<vector<char> > TBlock::getBlock() {
  return this->v;
}

void TBlock::print() {
  for(int i = 0 ; i < 4; i ++) {
     for(int j = 0 ; j < 4 ; j++) {
         cout << v[i][j];
      }
    std::cout << std::endl;
   }

}
void TBlock::setHeavy() {
  heavy = true;
}
void TBlock::unsetHeavy() {
  heavy = false;
}

bool TBlock::isHeavy() {
  return heavy;
}

void TBlock::setHeavyL3() {
  heavyL3 = true;
}

void TBlock::unsetHeavyL3() {
  heavyL3 = false;
}

bool TBlock::isHeavyL3() {
  return heavyL3;
}

int TBlock::rotateState() {
  return rotation;
}

void TBlock::setrotateState(int state ) {
  rotation = state;
}
TBlock::~TBlock() {v.clear();}
