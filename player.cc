#include <iostream>
#include <vector>
#include <string>
#include "player.h"
#include "cell.h"
#include "block.h"

void Player::setPlayerNum(int num) {

  playerNum = num;
}
void Player::setLev(int l) {
   level = l;
}
int Player::getLev() {
  return level;
}

int Player::getPlayerNum(){
  return playerNum;
}
Player::~Player(){
for(int i = 0 ; i < 18; i++) {
  for(int j = 0 ; j < 11 ; j++) {
     delete vc[i][j];
   }
}
}


bool Player::checkLeft() {
  int valid = 1;
  int counter = 0;
  for (int i = 0; i < 18 ;i++) {
    for (int j = 0; j < 11; j++) {
      if ( (!(vc[i][j]->checkEmpty()) && !(vc[i][j]->checkLanded()))) {
        if (j == 0) {//column is zero
          valid++;
          return false;
        }
        else if(vc[i][j-1]->checkLanded()) {
          valid++;
          return false;
        } else { counter++; }
      }
    }
   if(counter == 4) { return true;}
  }
  if (valid == 1) {
    return true;
  }
  return false;
}

bool Player::checkRight() {
  int valid = 1;
  int counter = 0;
  for (int i = 0; i < 18; i++) {
    for (int j = 0; j < 11; j++) {
      if ( (!vc[i][j]->checkEmpty() && !(vc[i][j]->checkLanded()))) {
        if (j == 10) {//column is 17
          valid++;
          return false;
        }
        else if(vc[i][j+1]->checkLanded()) {
          valid++;
          return false;
        } else { counter++; }
      }
    }
    if (counter == 4) { return true; }
  }
  if (valid == 1) {
    return true;
  }
  return false;
}

bool Player::checkDown() {
  int valid = 1;
  int counter = 0;
  for (int i = 0; i < 18; i++) {
    for (int j = 0; j < 11; j++) {
      if ( (!vc[i][j]->checkEmpty() && !(vc[i][j]->checkLanded()))) {
        if (i == 17) {
          valid++;
          return false;
        }
        else if(vc[i+1][j]->checkLanded()) {
          valid++;
          return false;
        } else { counter++; }
      }
    }
    if (counter == 4) { return true; }
  }
  if (valid == 1) {
    return true;
  }
  return false;
}

