#include "level1.h"
#include "block.h"
#include "iblock.h"
#include "jblock.h"
#include "lblock.h"
#include "oblock.h"
#include "sblock.h"
#include "tblock.h"
#include "zblock.h"
#include "player.h"
#include <fstream>
#include <iostream>
#include "level.h"
#include <cstdlib>
#include <memory>

using namespace std;

Level1::Level1(int s): s(s) {srand(s);}

void Level1::setLevel() { l = 1;}

int Level1::getLevel() {
    return l;
}

shared_ptr<Block> Level1::getBlock(Player *p) {
    shared_ptr<Block> b = nullptr;
    int r = rand() % 12;
    if (r == 0) {
        b = make_shared<SBlock>();
    }
    else if (r == 1) {
        b = make_shared<ZBlock>();
    }
    if (r == 2 || r == 3) {
        b = make_shared<IBlock>();
    }
    else if(r == 4 || r == 5) {
        b = make_shared<JBlock>();
    }
    else if(r == 6 || r == 7) {
        b = make_shared<LBlock>();
    }
    else if(r == 8 || r == 9) {
        b = make_shared<OBlock>();
    }
    else {
        b = make_shared<TBlock>();
    }
    return b;
}

Level1::~Level1() {}



