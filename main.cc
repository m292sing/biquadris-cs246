#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <vector>
#include <sstream>
#include <fstream>
#include "board.h"
#include "cell.h"
#include "player.h"
#include "level.h"
#include "iblock.h"
#include "jblock.h"
#include "sblock.h"
#include "zblock.h"
#include "oblock.h"
#include "tblock.h"
#include "lblock.h"
#include <cstdlib>

using namespace std;



int main (int argc, const char *argv[]) {

     vector <string> com = {
         "left", "right", "down", "drop", "clockwise",
         "counterclockwise", "levelup", "leveldown", 
         "norandom", "random", "sequence", "restart",
         "I", "J", "L", "O", "S", "Z", "T", "blind", "force","heavy"
     };

     vector <string> seqvec;

     Board *b = new Board();
     Player *p1 = b->getPlayer(1);
     Player *p2 = b->getPlayer(2);
     string command;
     string newfile1;
     string newfile2;
     string scriptfile1 = "biquadris_sequence1.txt";
     string scriptfile2 = "biquadris_sequence2.txt";
     bool sequencefile = false;
     int counter = 0;
     int l = 0;
     int s = 10;
     for (int i = 1; i < argc; ++i) {//command line interface
         command = argv[i];
         if (command == "-text") {
              b->mode = "text";
         }
         else if (command == "-seed") {
             stringstream(argv[i+1]) >> s;
         }
         else if (command == "-scriptfile1") {
             newfile1 = argv[i+1];
             scriptfile1 = newfile1;
             std::cout << scriptfile1;
         }
         else if (command == "-scriptfile2") {
             newfile2 = argv[i+1];
             scriptfile2 = newfile2;
             std::cout << scriptfile2;
         }
         else if (command == "-startlevel") {
             stringstream (argv[i+1]) >> l;
         }
     }
    // Requires level check here!
     b->setPlayer(p1,l,scriptfile1,s);
     b->setPlayer(p2,l,scriptfile2,s);

     p1->current = p1->l->getBlock(p1);
     p1->next = p1->l->getBlock(p1);

     p2->current = p2->l->getBlock(p2);
     p2->next = p2->l->getBlock(p2);

     if(b->mode == "text") {
      b->drawBlock(p1);
      b->drawBlock(p2);

     }
     b->drawBoard(p1,p2);
//     //pass the level as l, seed as s, scriptfile1, scriptfile2, boolean for textmode
//     //b{scriptfile1, scriptfile2, l, s, textmode};

     while (1) {
         string input;
         int num = 1;
         if (!sequencefile) {
          cin >> input;
          if(cin.eof()) {
            break;
          }
         }
         else {
           int max = seqvec.size();
           if (counter == max) {
             return 0;
           }
           input = seqvec.at(counter);
           counter+=1;
         }
         if (isdigit(input[0])) {
             num = input[0] - '0';
             input.erase(0,1);//if input[0] is an int, remove it
         }
 
             if (input.substr(0,3) == "lef") {
               if(b->currPlayer() == 1) {
                 for(int i = 0 ; i < num  ; i++) {
                   b->moveLeft(p1);
                 }
               } else {
                 for(int i = 0 ; i < num ; i++) {
                  b->moveLeft(p2);
               }
             }
             b->drawBoard(p1,p2);
             continue;

             } else if(input.substr(0,2) == "ri") {
               if(b->currPlayer() == 1) {
                 for(int i = 0 ; i < num; i++) {
                  b->moveRight(p1);
                 }
               } else {
                 for(int i = 0 ; i < num ; i++) {
                  b->moveRight(p2);
               }
             }
            // b->moveRight(p1);
             b->drawBoard(p1,p2);
             continue;
       
             }
             else if(input.substr(0,2) == "re") {
                 b->restart(p1,p2,scriptfile1,scriptfile2,0,s);
                  if(b->mode == "text") {
                      b->drawBlock(p1);
                      b->drawBlock(p2);

                   }
                    b->drawBoard(p1,p2);
                 continue;

             }
            else if(input.substr(0,2) == "do") {
              if(b->currPlayer() == 1) {
                for(int i = 0 ; i < num; i++) {
                 b->moveDown(p1);
               }
             } else {
               for(int i = 0 ; i < num; i++) {
                b->moveDown(p2);
               }
             }
             b->drawBoard(p1,p2);
             continue;

             }
            else if(input.substr(0,2) == "cl") {
                if (b->currPlayer() == 1) {
                  b->Rotatecw(p1);
                  b->drawBoard(p1,p2);
                  }
                else if (b->currPlayer() == 2) {
                  b->Rotatecw(p2);
                  b->drawBoard(p1,p2);
                }
            }
            else if (input.substr(0,2) == "co") {
              if (b->currPlayer() == 1) {
                  b->Rotateccw(p1);
                  b->drawBoard(p1,p2);
                  }
                else if (b->currPlayer() == 2) {
                  b->Rotateccw(p2);
                  b->drawBoard(p1,p2);
                }  
            }
             else if(input.substr(0,6) == "levelu") {
               if(b->currPlayer() == 1) {
                 b->setLevelup(p1,s,newfile1);
               } else{
                 b->setLevelup(p2,s,newfile2);
               }
              b->drawBoard(p1,p2);
              continue;
             } else if(input.substr(0,6) == "leveld") {
                if(b->currPlayer() == 1) {
                 b->setLeveldown(p1,s,scriptfile1);
               } else{
                 b->setLeveldown(p2,s,scriptfile2);
               }
               b->drawBoard(p1,p2);
               continue;
             }
             else if(input.substr(0,2) == "dr") {//drop
               if(b->currPlayer() == 1) {
                 b->Drop(p1);
                 p1->current = p1->next;            
                 p1->next = p1->l->getBlock(p1);
                 if(!b->drawBlock(p1) || b->isGameOver(p1)) {

                    b->drawBoard(p1,p2);

                     cout << "Player 2 wins. Enjoy!" << endl;

                     if(b->gameOver(p1,p2)){
                       b->restart(p1,p2,scriptfile1,scriptfile2,0,s);
                       b->drawBlock(p1);
                       b->drawBlock(p2);
                       b->drawBoard(p1,p2);
                       continue;

                     } else {
                       delete b;
                       return 0;
                     }
                 } 
               } else{
                 b->Drop(p2);
                 p2->current = p2->next;            
                 p2->next = p2->l->getBlock(p2);
                 if(!b->drawBlock(p2) || b->isGameOver(p2)) {

                    b->drawBoard(p1,p2);

                     cout << "Player 1 wins. Enjoy!" << endl;

                     if(b->gameOver(p1,p2)){
                       b->restart(p1,p2,scriptfile1,scriptfile2,0,s);
                       b->drawBlock(p1);
                       b->drawBlock(p2);
                       b->drawBoard(p1,p2);
                       continue;

                     } else {
                       delete b;
                       return 0;
                     }
                 } 

               }

              if (b->currPlayer() == 1) {
                b->unsetBlind(p2);
              } else {
               b->unsetBlind(p1);
              }

              b->turn = b->turn + 1;
              b->drawBoard(p1,p2); 
 
            if (p1->count_lines >= 2 || p2->count_lines >= 2) {
              cout << "Special Effects Activated.Which action? (blind,force,heavy)" << endl;
              continue;
            } 
            p1->count_lines = 0;
            p2->count_lines = 0;
             continue;
                 //drop
          } else if(input[0] == 'h' && (p1->count_lines >=2 || p2->count_lines >= 2))  {
            p1->count_lines = 0;
            p2->count_lines = 0;

            if(b->currPlayer() == 1) {
              b->makeblockHeavy(p1);

            } else {
              b->makeblockHeavy(p2);
            }
           b->drawBoard(p1,p2);
           continue;

          } else if(input[0] == 'b' && (p1->count_lines >=2 || p2->count_lines >= 2)) {
              p1->count_lines = 0;
              p2->count_lines = 0; 
              if (b->currPlayer() == 1) {
                b->setBlind(p1);
              } else {
                b->setBlind(p2);
             }
            b->drawBoard(p1,p2);
            continue;

          } else if(input[0] == 'f' && (p1->count_lines >=2 || p2->count_lines >= 2)) {
            p1->count_lines = 0;
            p2->count_lines = 0;
            cout << "Which block do you want to force?" << endl;
            char bname;
            cin >> bname;
            shared_ptr<Block> bl = nullptr;
            switch(bname) {
              case 'I': bl = make_shared<IBlock>(); break;
              case 'J': bl = make_shared<JBlock>(); break;
              case 'S': bl = make_shared<SBlock>(); break;
              case 'Z': bl = make_shared<ZBlock>(); break;
              case 'T': bl = make_shared<TBlock>(); break;
              case 'O': bl = make_shared<OBlock>(); break;
              case 'L': bl = make_shared<LBlock>(); break;
            }
            if (b->currPlayer() == 1) {
              b->undrawBlock(p1);
              p1->current = bl;
              b->drawBlock(p1);
            }
            if (b->currPlayer() == 2) {
              b->undrawBlock(p2);
              p2->current = bl;
              b->drawBlock(p2);
            }
            b->drawBoard(p1,p2);
            continue;

          } else if(input[0] == 'n') {//norandom file
                 string ranfile;
                 cin >> ranfile;

             if (b->currPlayer()==1) {
                 int level = p1->getLev();
                 if (level == 3 || level == 4) {
                   //srand(s);
                   p1->not_random = true;
                   p1->setLev(level - 1);
                   b->setLevelup(p1,s,ranfile);
                   b->drawBoard(p1,p2);
                 } else {
                   cout << "Level 3 and 4 only supported." << endl;
                   }
               } else if (b->currPlayer() == 2) {
                 int level = p2->getLev();
                 if (level == 3 || level == 4) {
                   p2->not_random = true;
                   p2->setLev(level-1);
                   b->setLevelup(p2,s,ranfile);
                   b->drawBoard(p1,p2);
                }
                else {
                  cout << "Level 3 and 4 only supported." << endl;
                }
               }

          } else if (input.substr(0,2) == "ra") {
               if (b->currPlayer()==1) {
                 int level = p1->getLev();
                 if (level == 3 ||  level == 4) {
                   p1->not_random = false;
                   srand(s);
                   p1->setLev(level - 1);
                   b->setLevelup(p1,s,scriptfile1);
                   b->drawBoard(p1,p2);
                 } else {
                   cout << "Level 3 and 4 only supported." << endl;
                   }
               } else if (b->currPlayer() == 2) {
                 int level = p2->getLev();
                 if (level == 3 || level == 4) {
                   p1->not_random = false;
                   srand(s);
                   p2->setLev(level-1);
                   b->setLevelup(p2,s,scriptfile2);
                   b->drawBoard(p1,p2);
                }
                else {
                  cout << "Level 3 and 4 only supported." << endl;
                }
               }
        }
        else if (input.substr(0,2) == "se") {
          sequencefile = true;
          string seqfile;
          cin >> seqfile;
          ifstream sequence(seqfile);
          string word;
            while (sequence >> word) {
              seqvec.push_back(word);
            }
          continue;
        } else if(input == "I") {
          shared_ptr<Block> bl = nullptr;
          bl = make_shared<IBlock>();
          if (b->currPlayer() == 1) {
              b->undrawBlock(p1);
              p1->current = bl;
              b->drawBlock(p1);
            }
            if (b->currPlayer() == 2) {
              b->undrawBlock(p2);
              p2->current = bl;
              b->drawBlock(p2);
            }
            b->drawBoard(p1,p2);
           continue; 
                 //Iblock
        } else if(input == "J") {
                 //Jblock
          shared_ptr<Block> bl = nullptr;
          bl = make_shared<JBlock>();
          if (b->currPlayer() == 1) {
              b->undrawBlock(p1);
              p1->current = bl;
              b->drawBlock(p1);
            }
            if (b->currPlayer() == 2) {
              b->undrawBlock(p2);
              p2->current = bl;
              b->drawBlock(p2);
            }
            b->drawBoard(p1,p2);
           continue; 
        } else if(input == "L") {
                 //Lblock
          shared_ptr<Block> bl = nullptr;
          bl = make_shared<LBlock>();
          if (b->currPlayer() == 1) {
              b->undrawBlock(p1);
              p1->current = bl;
              b->drawBlock(p1);
            }
            if (b->currPlayer() == 2) {
              b->undrawBlock(p2);
              p2->current = bl;
              b->drawBlock(p2);
            }
            b->drawBoard(p1,p2);
           continue; 
        } else if(input == "O") {
          
          shared_ptr<Block> bl = nullptr;
          bl = make_shared<OBlock>();
          if (b->currPlayer() == 1) {
              b->undrawBlock(p1);
              p1->current = bl;
              b->drawBlock(p1);
            }
            if (b->currPlayer() == 2) {
              b->undrawBlock(p2);
              p2->current = bl;
              b->drawBlock(p2);
            }
            b->drawBoard(p1,p2);
           continue;        //Oblock
        } else if(input == "S") {
          shared_ptr<Block> bl = nullptr;
          bl = make_shared<SBlock>();
          if (b->currPlayer() == 1) {
              b->undrawBlock(p1);
              p1->current = bl;
              b->drawBlock(p1);
            }
            if (b->currPlayer() == 2) {
              b->undrawBlock(p2);
              p2->current = bl;
              b->drawBlock(p2);
            }
            b->drawBoard(p1,p2);
           continue;        //Sblock
        } else if(input == "Z") {
           shared_ptr<Block> bl = nullptr;
          bl = make_shared<ZBlock>();
          if (b->currPlayer() == 1) {
              b->undrawBlock(p1);
              p1->current = bl;
              b->drawBlock(p1);
            }
            if (b->currPlayer() == 2) {
              b->undrawBlock(p2);
              p2->current = bl;
              b->drawBlock(p2);
            }
            b->drawBoard(p1,p2);
           continue;       //Jblock
        } else if(input == "T") {
            shared_ptr<Block> bl = nullptr;
          bl = make_shared<TBlock>();
          if (b->currPlayer() == 1) {
              b->undrawBlock(p1);
              p1->current = bl;
              b->drawBlock(p1);
            }
            if (b->currPlayer() == 2) {
              b->undrawBlock(p2);
              p2->current = bl;
              b->drawBlock(p2);
            }
            b->drawBoard(p1,p2);
           continue;      //Tblock
        }
        else {
           std::cout << "Invalid Command" << std::endl;
           std::cout << "Try again: " << std::endl;
           //break;
           continue;
           //break;
                 //error invalid input
        }
//         }
     }
     delete b;
}




