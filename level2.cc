#include "level2.h"
#include "block.h"
#include "iblock.h"
#include "jblock.h"
#include "lblock.h"
#include "oblock.h"
#include "sblock.h"
#include "tblock.h"
#include "zblock.h"
#include "player.h"
#include <fstream>
#include <iostream>
#include "level.h"
#include <cstdlib>
#include <memory>

using namespace std;

Level2::Level2(int s): s(s) {srand(s);}

void Level2::setLevel() { l = 2;}

int Level2::getLevel() {
    return l;
}

shared_ptr<Block> Level2::getBlock(Player *p) {
    shared_ptr<Block> b = nullptr;
    int r = rand() % 7;
    if (r == 0) {
        b = make_shared<SBlock>();
    }
    else if (r == 1) {
        b = make_shared<ZBlock>();
    }
    else if (r == 2) {
        b = make_shared<IBlock>();
    }
    else if(r == 3) {
        b = make_shared<JBlock>();
    }
    else if(r == 4) {
        b = make_shared<LBlock>();
    }
    else if(r == 5) {
        b = make_shared<OBlock>();
    }
    else {
        b = make_shared<TBlock>();
    }
    return b;
}

Level2::~Level2() {}
