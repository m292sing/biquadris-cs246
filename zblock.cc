#include <iostream>
#include <string>
#include <vector>
#include "zblock.h"
using namespace std;

ZBlock::ZBlock() {
    heavy = false;

    rotation = 0;
    block_type = 'Z';
    for(int i = 0 ; i < 4; i ++) {
    std::vector<char> c2;
     for(int j = 0 ; j < 4 ; j++) {
         if (i == 2 && (j == 0 || j == 1)) {
             c2.push_back('Z');
         }
         else if (i == 3 && (j == 1 || j == 2)) {
             c2.push_back('Z');
         }
         else {
             c2.push_back(' ');
        }
      }
      v.push_back(c2);
   }
}

char ZBlock::getName() { return block_type;}

vector<vector<char> > ZBlock::getBlock() {
  return this->v;
}

void ZBlock::print() {
  for(int i = 0 ; i < 4; i ++) {
     for(int j = 0 ; j < 4 ; j++) {
         cout << v[i][j];
      }
    std::cout << std::endl;
   }

}
void ZBlock::setHeavy() {
  heavy = true;
}
void ZBlock::unsetHeavy() {
  heavy = false;
}

bool ZBlock::isHeavy() {
  return heavy;
}

void ZBlock::setHeavyL3() {
  heavyL3 = true;
}

void ZBlock::unsetHeavyL3() {
  heavyL3 = false;
}

bool ZBlock::isHeavyL3() {
  return heavyL3;
}

int ZBlock::rotateState() {
  return rotation;
}

void ZBlock::setrotateState(int state ) {
  rotation = state;
}
ZBlock::~ZBlock() {v.clear();}
